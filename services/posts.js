const Mongo = require('../lib/mongo');

class PostsService {
    constructor() {
        this._collection = 'posts';
        this._db = new Mongo();
    }

    async findById(id) {
        const post = await this._db.get(this._collection, id);
        return post;
    }
}

module.exports = PostsService;