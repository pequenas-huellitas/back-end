const Mongo = require('../lib/mongo');

class UserService {
    constructor() {
        this._collection = 'users';
        this._mongo = new Mongo();
    }

    async getUser() {

    }

    async createUser() {

    }

    async updateUser() {

    }

    async delete()
    {

    }
}

module.exports = UserService;
