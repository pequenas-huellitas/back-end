const Mongo = require('../lib/mongo');

class DogsService {
    constructor() {
        this._collection = 'dogs';
        this._mongo = new Mongo();
    }

    async getDogs() {
        return await this._mongo.getAll(this._collection, {});
    }

    async getDog(id) {
        return await this._mongo.get(this._collection, id);
    }

    async createDog(dog) {
        return await this._mongo.create(this._collection, dog);
    }

    async updateDog({ id, dog } = {}) {
        return await this._mongo.update(this._collection, id, dog);
    }

    async delete({id}) {
        return await this._mongo.delete(this._collection, id)
    }
}

module.exports = DogsService;
