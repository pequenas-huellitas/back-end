# Pequeñas Huellitas backend

This NodeJS backend to support Pequeñas Huellitas site.

## Description

It's a site to help the community to change the culture to adopt homeless dogs, we want to increase this culture around the world, so if you can support this project you are welcome to collaborate.

## Getting Started

### Dependencies

* express
* dotenv

### Installing

```
npm install
```

### Executing program

* This project just has development mode at least.

```
npm run dev
```

## Authors

Contributors names and contact info

Alejandro Cortez  [@ziker_on](https://twitter.com/ziker_on)  
Jóse Luis Garcia  
Francisco Gustavo  

## Version History

* 1.0.0
    * Initial Release

## License

This project is licensed under the MIT License
