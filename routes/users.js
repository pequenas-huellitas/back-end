const express = require('express');
const validationHandler = require('../utils/middleware/validationHandler');
const userService = require('../services/users');
const {
    userIdSchema,
    createUsersSchema,
    createAssociationsSchema
} = require('../utils/schemas/users');

function usersApi(app)
{
    const userService = new userService();
    const router = express.Router();
    app.use('/api/users', router);

    router.get('/:userId',
        validationHandler({ userId:userIdSchema}, 'params'),
        async function (req, res, next) {
            const { userId } = req.params;
            try {
               const userfetcher = await userService.getUser();
               res.status(200).json({
                   data: userfetcher,
                   result: 0,
               })
            } catch (e) {
                next(e)
            }
        })
}
