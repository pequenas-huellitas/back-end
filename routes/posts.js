const express = require('express');
const PostsService = require('../services/posts');
const validationHandler = require('../utils/middleware/validationHandler');
const { postIdSchema } = require('../utils/schemas/blogs');

function postsApi(app) {
  const _postsService = new PostsService();
  const router = express.Router();
  app.use('/api/posts', router);

  router.get('/:id', 
    validationHandler({ id: postIdSchema }, 'params'),
    async function (req, res, next) {
      const postId = req.params.id;
      try {
        const post = await _postsService.findById(postId);
        res.status(200).json({
          message: 'posts ready',
          data: post,
        });
      } catch (error) {
        next(error);
      }
    }
  );
}

module.exports = postsApi;