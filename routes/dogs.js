const express = require('express');
const DogsService = require('../services/dogs');
const {
  dogIdSchema,
  createDogSchema
} = require('../utils/schemas/dogs');

const validationHandler = require('../utils/middleware/validationHandler');
const dogServ = new DogsService();

function dogsApi(app) {
  const router = express.Router();
  app.use('/api/dogs', router);

  router.get('/', async function (req, res, next) {
    await dogServ.getDogs().then(dogs => {
      res.status(200).json({
        data: dogs,
        message: 'dogs retrieved',
        status: 0
      });
    }).catch((error) => {
      next(error);
    });
  });

  router.get('/:dogId', validationHandler({dogId: dogIdSchema}, 'params'), async function (req, res, next) {
    const {dogId} = req.params;
    await dogServ.getDog(dogId)
      .then(dog => {
        res.status(200).json({
          data: dog,
          message: 'dog retrieved',
          status: 0
        });
      })
      .catch(error => {
        next(error);
      });
  });

  router.post('/', validationHandler(createDogSchema), async function (req, res, next) {
    await dogServ.createDog(req.body)
      .then(data => {
        res.status(201).json({
          data: data,
          message: 'dog created',
          result: 0
        });
      })
      .catch(error => {
        next(error);
      });
  });

  router.put('/',  async function (req, res, next) {
    const { id } = req.body;
    let dog = req.body;

    if (id && new RegExp(/^[0-9a-fA-F]{24}$/).test(id)) {
      delete dog.id;
    } else {
      res.status(400).json({
        message: 	"ID not found or unrecognized",
        result: 2
      }).status(400);
    }

    await dogServ.updateDog({id, dog})
      .then(data => {
        res.status(200).json({
          data: data,
          message: 'dog updated',
          result: 0
        });
      })
      .catch(error => {
        next(error);
      });
  });

  router.delete('/',  async function (req, res, next) {
    const { id } = req.body;

    if (!id || !(new RegExp(/^[0-9a-fA-F]{24}$/).test(id))) {
      res.status(400).json({
        message: "ID not found or unrecognized",
        result: 2
      }).status(400);
    }
    console.log('id',req.body)
    await dogServ.delete({id})
      .then(data => {
        res.status(200).json({
          data: data,
          message: 'dog deleted',
          result: 0
        });
      })
      .catch(error => {
        next(error);
      });
  });
}

module.exports = dogsApi;
