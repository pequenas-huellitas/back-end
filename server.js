const express = require('express');
const morgan = require('morgan');
const {config} = require('./config/index')
const dogsApi = require('./routes/dogs');
const postsApi = require('./routes/posts');
const app = express();

const {
  logErrors,
  wrapErrors,
  errorHandler
} = require('./utils/middleware/errorHandlers');

const notFoundHandler = require('./utils/middleware/notFoundHandler');

// body parser
app.use(express.json());
app.use(morgan('dev'));

// routes
app.get('/', (req, res, next) => {
  try {
    res.status(200)
    .send({
      "api":"Pequeñas Huellitas Backend",
      "version": "1.0.0",
    });

  } catch (error) {
    next(error);
  }
});
dogsApi(app);
postsApi(app);

// 404 error
app.use(notFoundHandler);

// Errors middleware
app.use(logErrors);
app.use(wrapErrors);
app.use(errorHandler);

const server = app.listen(config.server.port, () => {
    console.log(`Server is listening at http://localhost:${server.address().port}`);
});
