const joi = require('@hapi/joi');

const dogIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);
const dogNameSchema = joi.string().max(50);
const dogBreedSchema = joi.string().max(50);
const dogAgeSchema = joi.number();
const dogWeightSchema = joi.number();
const dogColorSchema = joi.string().max(50);
const dogHeigthSchema = joi.number();
const dogDescriptionSchema = joi.string().max(500);

const createDogSchema = {
  name: dogNameSchema.required(),
  breed: dogBreedSchema,
  age: dogAgeSchema,
  weight: dogWeightSchema,
  color: dogColorSchema.required(),
  height: dogHeigthSchema,
  description: dogDescriptionSchema.required()
}

module.exports = {
  dogIdSchema,
  createDogSchema
}
