const joi = require('@hapi/joi');

const userIdSchema = joi.string().regex(/^[0-9a-fA-F]{24}$/);

const usersSchema = {
    name: joi.string().max(100).required(),
    email: joi.string().email().required(),
    password: joi.string().required(),
    avatar: joi.string().uri(),
    birthday: joi.date(),
    address: joi.string(),
    location: joi.string(),
    phone: joi.string(),
    isAssociation: joi.boolean()
};

const createUsersSchema = {
    ...usersSchema,
};

const createAssociationsSchema = {
    ...usersSchema,
    association: joi.boolean(),
    mission: joi.string().required(),
    vision: joi.string().required(),
    website: joi.string().uri(),
};

module.exports = {
    userIdSchema,
    createUsersSchema,
    createAssociationsSchema
};
