require('dotenv').config();

const config = {
    server: {
        dev: process.env.NODE_ENV !== 'production',
        port: process.env.PORT || 3000,
    },
    db: {
        host: process.env.DB_HOST,
        name: process.env.DB_NAME,
        user: process.env.DB_USER,
        pass: process.env.DB_PASS,
    }
}

module.exports = {config};
