const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const dogSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    breed: String,
    age: Number,
    weight: Number,
    color: {
        type: String,
        required: true,
    },
    height: Number,
    description: {
        type: String,
        required: true
    }
});

const Dogs = mongoose.model('Dogs', dogSchema);

module.exports = Dogs;
