const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const usersSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    phone: Number,
    photo: String,
    address: String,
    description: String,
});

const Users = mongoose.model('Users', usersSchema);

module.exports = Users;