const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postsSchema = new Schema({
    author: {
        type: Schema.Types.ObjectId,
        ref: 'Users',
        required: true
    },
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true,
    },
    keywords: String,
    description: String,
    public: Boolean,
    likes: Number,
});

const Posts = mongoose.model('Posts', postsSchema);

module.exports = Posts;